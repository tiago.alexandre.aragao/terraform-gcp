terraform {
    backend "gcs" {
        bucket="bucket-gcp-terraform-gitlab"
    }
}

provider "google" {
    project="homologacao01"
    region=var.region
}
